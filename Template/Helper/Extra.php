<?php

class ThreadPrefixHelper_Template_Helper_Extra
{

	/**
	 * List of thread prefixes for marking up threads.
	 *
	 * @var array [prefix id] => css class name
	 */
	protected static $_threadPrefixes = array();

	/**
	* Private constructor. Don't instantiate this object. Use it statically.
	*/
	private function __construct() {}


	public static function helperThreadPrefixExtra($prefixId, $outputType = 'html', $prepend = null, $append = null, $textPrepend = '[', $textAppend = ']')
	{
		if (is_array($prefixId))
		{
			if (!isset($prefixId['prefix_id']))
			{
				return '';
			}

			$prefixId = $prefixId['prefix_id'];
		}

		$prefixId = intval($prefixId);
		if (!$prefixId || !isset(self::$_threadPrefixes[$prefixId]))
		{
			return '';
		}

		$text = self::_getPhraseText('thread_prefix_' . $prefixId);
		if ($text === '')
		{
			return '';
		}

		switch ($outputType)
		{
			case 'html':
				$text = '<span class="' . htmlspecialchars(self::$_threadPrefixes[$prefixId]) . '">' . $textPrepend
					. htmlspecialchars($text) . $textAppend . '</span>';
				if ($append === null)
				{
					$append = ' ';
				}
				if ($prepend === null)
				{
					$prepend = ' ';
				}
				break;

			case 'plain':
				$text = $textPrepend . $text . $textAppend;
				break;

			default:
				$text = $textPrepend . htmlspecialchars($text) . $textAppend; // just be safe and escape everything else
		}

		if ($append === null)
		{
			$append = ' - ';
		}

		return $prepend . $text . $append;
	}

	/**
	 * Fetches the text of the specified phrase
	 *
	 * @param string $phraseName
	 *
	 * @return string
	 */
	protected static function _getPhraseText($phraseName)
	{
		$language = XenForo_Template_Helper_Core::getDefaultLanguage();

		if ($language && isset($language['phrase_cache']))
		{
			$cache = $language['phrase_cache'];
			return (isset($cache[$phraseName]) ? $cache[$phraseName] : '');
		}
		else
		{
			$phrase = new XenForo_Phrase($phraseName);
			return $phrase->render(false);
		}
	}

	/**
	 * Sets the thread prefixes.
	 *
	 * @param array $prefixes [prefix id] => class name
	 */
	public static function setThreadPrefixes($prefixes)
	{
		self::$_threadPrefixes = $prefixes;
	}
}
