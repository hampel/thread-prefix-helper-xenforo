<?php

class ThreadPrefixHelper_Listener_InitDependencies
{
	public static function initDependencies(XenForo_Dependencies_Abstract $dependencies, array $data)
	{
		if ($dependencies instanceof XenForo_Dependencies_Public && isset($data['threadPrefixes']))
		{
			ThreadPrefixHelper_Template_Helper_Extra::setThreadPrefixes($data['threadPrefixes']);
		}

		XenForo_Template_Helper_Core::$helperCallbacks['threadprefixextra'] = [
			'ThreadPrefixHelper_Template_Helper_Extra',
			'helperThreadPrefixExtra'
		];
	}
}
